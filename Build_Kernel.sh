#!/bin/bash

if [ ! -d `pwd`/tmpWorkingFolder ]; then
	mkdir tmpWorkingFolder
fi

cd tmpWorkingFolder

if [ ! -d `pwd`/gcc-arm-none-eabi-4_8-2013q4 ]; then
	wget -c https://launchpad.net/gcc-arm-embedded/4.8/4.8-2013-q4-major/+download/gcc-arm-none-eabi-4_8-2013q4-20131204-linux.tar.bz2
	tar xf gcc-arm-none-eabi-4_8-2013q4-20131204-linux.tar.bz2
fi

export CC=`pwd`/gcc-arm-none-eabi-4_8-2013q4/bin/arm-none-eabi-

if [ ! -d `pwd`/linux-at91 ]; then
	git clone git://github.com/linux4sam/linux-at91.git
fi

cd linux-at91

git branch -a
read -p "Please enter the name of the branch you wish to use: " GIT_BRANCH
git checkout $GIT_BRANCH
git pull

ls arch/arm/configs/at91*
read -p "Please enter the name of the config you wish to use: " CONFIG_NAME

make clean

make ARCH=arm $CONFIG_NAME

make ARCH=arm menuconfig

make ARCH=arm CROSS_COMPILE=$CC zImage

# cat arch/arm/boot/zImage arch/arm/boot/dts/at91sam9g20ek_2mmc.dts > my-zImage
# mkimage -A arm -O linux -T kernel -C none -a 0x20008000 -e 0x20008000 -n Linux+dtb -d my-zImage uImage


# make ARCH=arm CROSS_COMPILE=$CC uImage

cd ..
cp linux-at91/arch/arm/boot/uImage `pwd`


# export CC=/home/kgmoney/Projects/Glomation/gesbc-9g20w-build-tools/test/gcc-arm-none-eabi-4_8-2013q4/bin/arm-none-eabi-gcc
# export AR=/home/kgmoney/Projects/Glomation/gesbc-9g20w-build-tools/test/gcc-arm-none-eabi-4_8-2013q4/bin/arm-none-eabi-ar
# export CXX=/home/kgmoney/Projects/Glomation/gesbc-9g20w-build-tools/test/gcc-arm-none-eabi-4_8-2013q4/bin/arm-none-eabi-g++
# export LINK=/home/kgmoney/Projects/Glomation/gesbc-9g20w-build-tools/test/gcc-arm-none-eabi-4_8-2013q4/bin/arm-none-eabi-g++



